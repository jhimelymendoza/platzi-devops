# stage 1
FROM node:14.17.0 as node
WORKDIR /app
COPY . .
RUN npm install
RUN npm run build:ssr
CMD ["node", "dist/gitlab-devops-angular/server/main.js"]

# stage 2
#FROM nginx:alpine
#COPY --from=node /app/dist/gitlab-devops-angular /usr/share/nginx/html


